import { HttpClient } from '@angular/common/http';
import { Profile } from '../interfaces/profile';
import { Observable } from 'rxjs';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private api = '/api';
  private _profile: Profile;

  constructor(private restAuth: RestAuthService, private http: HttpClient) {}

  getBathOptions() {
    const url = `${this.api}/api/v1/options/`;
    return this.http.get(url);
  }

  getBathTypes() {
    const url = `${this.api}/api/v1/types_bath/`;
    return this.http.get(url);
  }

  getUser() {
    this.restAuth.getUserProfile().subscribe(data => (this._profile = data));
  }

  get userProfile() {
    return this._profile;
  }
}
