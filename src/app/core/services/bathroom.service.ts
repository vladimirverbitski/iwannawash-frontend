import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Bathroom } from '../interfaces/bathroom';
import { MessagesService } from 'src/app/shared/messages/messages.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class BathroomService implements OnInit {

  private bathroomsUrl = 'api/bathrooms';

  constructor(
    private http: HttpClient,
    private messagesService: MessagesService
  ) { }

  ngOnInit() {
    this.getBathrooms();
  }

  getBathrooms(): Observable<Bathroom[]> {
    return this.http.get<Bathroom[]>(this.bathroomsUrl)
      .pipe(
        tap(bathrooms => this.log('fetched bathrooms')),
        catchError(this.handleError('getBathrooms', []))
      );
  }

  getBathroom(id: number): Observable<Bathroom> {
    const url = `${this.bathroomsUrl}/${id}`;
    return this.http.get<Bathroom>(url).pipe(
      tap(_ => this.log(`fetched bathroom id = ${id}`)),
      catchError(this.handleError<Bathroom>(`getBathroom id = ${id}`))
    );
  }

  addBathroom(bathroom: Bathroom): Observable<Bathroom> {
    return this.http.post<Bathroom>(this.bathroomsUrl, bathroom, httpOptions).pipe(
      tap((bath: Bathroom) => this.log(`added hero w/ id=${bathroom.id}`)),
      catchError(this.handleError<Bathroom>('addBathroom'))
    );
  }

  updateBathroom(bathroom: Bathroom): Observable<any> {
    return this.http.put(this.bathroomsUrl, bathroom, httpOptions).pipe(
      tap(_ => this.log(`updated bathroom id=${bathroom.id}`)),
      catchError(this.handleError<any>('updateBathroom'))
    );
  }

  deleteBathroom(bathroom: Bathroom | number): Observable<Bathroom> {
    const id = typeof bathroom === 'number' ? bathroom : bathroom.id;
    const url = `${this.bathroomsUrl}/${id}`;

    return this.http.delete<Bathroom>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted hero id=${id}`)),
      catchError(this.handleError<Bathroom>('deleteBathroom'))
    );
  }

  searchBathrooms(term: string): Observable<Bathroom[]> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }
    return this.http.get<Bathroom[]>(`${this.bathroomsUrl}/?title=${term}`).pipe(
      tap(_ => this.log(`found bathrooms matching "${term}"`)),
      catchError(this.handleError<Bathroom[]>('searchBathrooms', []))
    );
  }

  getBathroomNo404<Data>(id: number): Observable<Bathroom> {
    const url = `${this.bathroomsUrl}/?id=${id}`;
    return this.http.get<Bathroom[]>(url)
      .pipe(
        map(bathrooms => bathrooms[0]),
        tap(b => {
          const outcome = b ? `fetched` : `did not find`;
          this.log(`${outcome} bathroom id = ${id}`);
        }),
        catchError(this.handleError<Bathroom>(`getBathroom id = ${id}`))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messagesService.add(`BathroomService: ${message}`);
  }

}
