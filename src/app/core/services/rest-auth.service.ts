import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginRequest } from 'src/app/core/interfaces/login-request';
import { User } from 'src/app/core/interfaces/user';
import { Profile } from 'src/app/core/interfaces/profile';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

const TOKEN = 'JWT';

@Injectable({
  providedIn: 'root',
})
export class RestAuthService {
  private restAuthUrl = '/api';

  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  setToken(token: string): void {
    localStorage.setItem(TOKEN, token);
  }

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  destroyToken(): void {
    localStorage.removeItem(TOKEN);
  }

  logout() {
    const url = `${this.restAuthUrl}/rest_auth/logout/`;
    return this.http.post(url, {}).pipe(
      map(() => this.destroyToken())
    ).subscribe( it => {
      this.router.navigateByUrl('/login');
    });
  }

  getUserProfile(): Observable<Profile> {
    const url = `${this.restAuthUrl}/rest_auth/user/`;
    return this.http.get<Profile>(url);
  }

  userLogin(user: User): Observable<LoginRequest> {
    const url = `${this.restAuthUrl}/rest_auth/login/`;
    return this.http.post<LoginRequest>(url, user);
  }

  updateProfile(profile: Profile): Observable<Profile> {
    const url = `${this.restAuthUrl}/rest_auth/user/`;
    return this.http.put<Profile>(url, profile);
  }

  uploadImages(images) {
    const url = `${this.restAuthUrl}/api/v1/user_bath_images/`;

    return this.http.post(url, images);
  }
}
