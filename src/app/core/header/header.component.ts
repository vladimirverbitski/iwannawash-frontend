import { Component, OnInit } from '@angular/core';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private restAuthService: RestAuthService
  ) { }

  ngOnInit() {

  }

  logout() {
    this.restAuthService.logout();
  }

  isUserAuthorized() {
    return this.restAuthService.isLogged();
  }

}
