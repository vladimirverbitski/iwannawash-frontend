export enum BathType {
  sauna = 'Сауга',
  shower = 'Душевая кабина',
  bath = 'Ванная',
  jakuzi = 'Джакузи',
}
