export class Profile {
  pk: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  area: string;
  description: string;
  date_start: Date;
  date_end: Date;
  bath_images: Array<BathImage>;
  options: Array<string>;
  type_bath: Array<string>;
}

export interface BathImage {
  pk: number;
  image: string;
}
