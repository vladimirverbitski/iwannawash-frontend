export enum BathOptions {
  fen = 'Фен',
  duck = 'Резиновая уточка',
  shampoo = 'Шампунь',
  gel = 'Гель для душа',
  sponge = 'Мочалка',
}
