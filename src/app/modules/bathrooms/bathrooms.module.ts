import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BathroomPreviewComponent } from './bathroom-preview/bathroom-preview.component';
import { BathroomSearchComponent } from './bathroom-search/bathroom-search.component';
import { BathroomsDetailComponent } from './bathroom-detail/bathrooms-detail.component';
import {BathroomsRoutingModule} from './bathrooms-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BathroomsRoutingModule
  ],
  declarations: [
    BathroomPreviewComponent,
    BathroomSearchComponent,
    BathroomsDetailComponent
  ]
})

export class BathroomsModule { }
