import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {BathroomSearchComponent} from './bathroom-search/bathroom-search.component';

const routes = [
  {
    path: '',
    component: BathroomSearchComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class BathroomsRoutingModule { }
