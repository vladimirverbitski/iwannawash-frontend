import { RestAuthService } from 'src/app/core/services/rest-auth.service';
import { ProfileService } from './../../core/services/profile.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import { Profile } from '../../core/interfaces/profile';
import { ActivatedRoute, Router } from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {

  profile: Profile;
  unsubscribe: Subject<void> = new Subject();

  constructor(
    private restAuth: RestAuthService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.data
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe((data: { profile: Profile }) => {
        this.profile = data.profile;
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  exit() {
    this.restAuth.logout();
  }

}
