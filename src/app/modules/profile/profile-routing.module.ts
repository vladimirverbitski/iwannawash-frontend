import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {ProfileComponent} from './profile.component';
import {ProfileResolverService} from './profile-resolver.service';

const routes = [
  {
    path: '',
    resolve: {
      profile: ProfileResolverService
    },
    component: ProfileComponent,
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [ProfileResolverService]
})
export class ProfileRoutingModule { }
