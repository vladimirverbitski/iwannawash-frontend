import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { MzDatepickerModule, MzInputModule } from 'ngx-materialize';
import {ProfileRoutingModule} from './profile-routing.module';
import {FileUploadComponent} from '../../shared/components/file-upload/file-upload.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MzDatepickerModule,
    MzInputModule,
    NgSelectModule,
    ProfileRoutingModule
  ],
  declarations: [ProfileComponent, ProfileEditComponent, FileUploadComponent],
})
export class ProfileModule {}
