import { Component, OnInit, Input } from '@angular/core';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';
import { Profile } from 'src/app/core/interfaces/profile';
import { FormGroup, FormControl } from '@angular/forms';
import { Subject} from 'rxjs';
import {ProfileService} from '../../../core/services/profile.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css'],
})
export class ProfileEditComponent implements OnInit {
  unsubscribe: Subject<void> = new Subject();
  @Input() profile: Profile;
  optionList;
  bathList;

  profileForm: FormGroup = new FormGroup({
    first_name: new FormControl(''),
    last_name: new FormControl(''),
    area: new FormControl(''),
    date_start: new FormControl(''),
    date_end: new FormControl(''),
    type_bath: new FormControl([]),
    options: new FormControl([]),
  });

  get firstName() {
    return this.profileForm.get('first_name');
  }

  get lastName() {
    return this.profileForm.get('last_name');
  }

  get area() {
    return this.profileForm.get('area');
  }

  get dateStart() {
    return this.profileForm.get('date_start');
  }

  get dateEnd() {
    return this.profileForm.get('date_end');
  }

  get bathType() {
    return this.profileForm.get('type_bath');
  }

  get bathOptions() {
    return this.profileForm.get('options');
  }

  constructor(
    private restAuthService: RestAuthService,
    private profileService: ProfileService
  ) {}

  ngOnInit() {
    this.fillProfile(this.profile);
    this.initBathOptions();
  }

  fillProfile(profile: Profile) {
    this.firstName.setValue(profile.first_name);
    this.lastName.setValue(profile.last_name);
    this.area.setValue(profile.area);
    this.dateStart.setValue(profile.date_start);
    this.dateEnd.setValue(profile.date_end);
    this.bathOptions.patchValue(profile.options);
    this.bathType.patchValue(profile.type_bath);
  }

  initBathOptions() {
    this.profileService.getBathOptions()
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe( data => this.optionList = data);

    this.profileService.getBathTypes()
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe( data => this.bathList = data);
  }

  updateProfile() {
    this.restAuthService.updateProfile(this.profileForm.value).subscribe(
      s => {
        console.log('hello');
      },
      e => {
        if (e.error.non_field_errors) {
          console.log(e.error.non_field_errors[0]);
        }
      }
    );
  }
}
