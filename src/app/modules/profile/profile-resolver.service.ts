import { RestAuthService } from './../../core/services/rest-auth.service';
import { Profile } from 'src/app/core/interfaces/profile';
import { Observable, of, EMPTY } from 'rxjs';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProfileService } from './../../core/services/profile.service';
import { Injectable } from '@angular/core';
import { take, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProfileResolverService {
  constructor(
    private profileService: ProfileService,
    private restAuth: RestAuthService,
    private router: Router
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Profile> {
    return this.restAuth.getUserProfile().pipe(
      take(1),
      mergeMap(user => {
        if (user) {
          return of(user);
        } else {
          this.router.navigate(['/']);
          return EMPTY;
        }
      })
    );
  }
}
