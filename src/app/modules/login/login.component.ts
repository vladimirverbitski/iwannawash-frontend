import { Component, OnInit } from '@angular/core';
import { User } from '../../core/interfaces/user';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private restAuthService: RestAuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.restAuthService.isLogged()) {
      this.router.navigateByUrl('/profile');
    }
  }

  login(loginForm: User) {
    this.restAuthService.userLogin(loginForm).subscribe(
      s => {
        if (s.token) {
          this.restAuthService.setToken(s.token);
          this.router.navigateByUrl('/profile');
        }
      },
      e => {
        if (e.error.non_field_errors) {
          alert(e.error.non_field_errors[0]);
        }
      }
    );
  }

}
