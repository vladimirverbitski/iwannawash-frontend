import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'bathrooms',
    children: [{
      path: '',
      loadChildren: () => import('./modules/bathrooms/bathrooms.module').then(mod => mod.BathroomsModule)
    }]
  },
  {
    path: 'profile',
    canActivate: [AuthGuard],
    children: [{
      path: '',
      loadChildren: () => import('./modules/profile/profile.module').then(mod => mod.ProfileModule)
    }]
  },
  {
    path: 'login',
    children: [{
      path: '',
      loadChildren: () => import('./modules/login/login.module').then(mod => mod.LoginModule)
    }]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class AppRoutingModule { }
