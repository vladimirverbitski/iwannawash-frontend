import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {RestAuthService} from '../../../core/services/rest-auth.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  file: File = null;

  constructor(
    private restAuthService: RestAuthService
  ) {}

  ngOnInit(): void {
  }

  handleFileInput(file: File) {
    this.file = file;
    this.uploadFileToActivity();
  }

  uploadFileToActivity() {
    const formData: FormData = new FormData();
    formData.append('image', this.file);

    this.restAuthService.uploadImages(formData).subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    });
  }

}
