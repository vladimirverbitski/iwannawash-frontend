import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesModule } from './messages/messages.module';
import { LayoutModule } from './layout/layout.module';

@NgModule({
  imports: [
    CommonModule,
    MessagesModule,
    LayoutModule
  ],
  declarations: [],
  exports: [
    LayoutModule,
    MessagesModule
  ]
})
export class SharedModule { }
