import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpHeaders
  } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';

@Injectable()
export class AuthTokenInterceptor implements HttpInterceptor {

    constructor(
      private restAuth: RestAuthService
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const currentUser = this.restAuth.isLogged;
      if (currentUser && localStorage.getItem('JWT') != null) {
          const token =  localStorage.getItem('JWT');
          request = request.clone({
            setHeaders: {
              Authorization: `JWT ${token}`
            }
          });
        }

      return next.handle(request);
    }
}
