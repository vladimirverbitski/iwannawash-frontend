import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { RestAuthService } from 'src/app/core/services/rest-auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private restAuth: RestAuthService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError( err => {
          if (err.status === 401) {
            console.log(err);
            localStorage.clear();
            this.restAuth.logout();
          }

          const error = err.error.message || err.statusText;
          return throwError(error);
        })
      );
  }
}
